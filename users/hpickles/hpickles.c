#include "hpickles.h"

enum custom_keycodes {
  KC_TMUX = SAFE_RANGE,
};

enum layers {
    _COLEMAK_DH,
    _NAV,
    _SYM,
    _FUNCTION,
    _MOUSE,
    // _ADJUST,
};

// Aliases for readability
#define COLEMAK  DF(_COLEMAK_DH)

#define NAV      MO(_NAV)
#define FKEYS    MO(_FUNCTION)
#define MOUSE    MO(_MOUSE)
//#define ADJUST   MO(_ADJUST)
// #define SYM_RBRC LT(_SYM, KC_RBRC)
#define SYM MO(_SYM)

#define CTL_ESC  MT(MOD_LCTL, KC_ESC)
#define CTL_QUOT MT(MOD_RCTL, KC_QUOTE)
#define CTL_MINS MT(MOD_RCTL, KC_MINUS)
#define ALT_ENT  MT(MOD_LALT, KC_ENT)

// Homerow Mods
#define LGUI_A LGUI_T(KC_A)
#define LALT_R LALT_T(KC_R)
#define LCTL_S LCTL_T(KC_S)
#define LSHFT_T LSFT_T(KC_T)

#define RSFT_N RSFT_T(KC_N)
#define RCTL_E RCTL_T(KC_E)
#define RALT_I RALT_T(KC_I)
#define RGUI_O RGUI_T(KC_O)

#ifdef LEADER_ENABLE

void leader_end_user(void){
    // Common phrases
    if (leader_sequence_two_keys(KC_D, KC_S)) {
      SEND_STRING("Dog's sleeping");
    } else if (leader_sequence_two_keys(KC_D, KC_W)) {
      SEND_STRING("We are doing well.");
    } else if (leader_sequence_one_key(KC_G)) {
      SEND_STRING("Geeze");
    } else if (leader_sequence_two_keys(KC_H, KC_D)) {
      SEND_STRING("How are you doing?");
    } else if (leader_sequence_two_keys(KC_H, KC_W)) {
      SEND_STRING("Hope you are doing well");
    } else if (leader_sequence_two_keys(KC_I, KC_I)) {
      SEND_STRING("Let me know if you notice any issues.");
    } else if (leader_sequence_two_keys(KC_J, KC_C)) {
      SEND_STRING("Just chillin");
    } else if (leader_sequence_one_key(KC_N)) {
      SEND_STRING("Nice");
    } else if (leader_sequence_two_keys(KC_N, KC_P)) {
      SEND_STRING("No problem");
    } else if (leader_sequence_two_keys(KC_S, KC_E)) {
      SEND_STRING("Super excited!!!");
    } else if (leader_sequence_two_keys(KC_S, KC_F)) {
      SEND_STRING("Sounds fun");
    } else if (leader_sequence_two_keys(KC_S, KC_G)) {
      SEND_STRING("Sounds good");
    } else if (leader_sequence_one_key(KC_T)) {
      SEND_STRING("Thanks");
    } else if (leader_sequence_two_keys(KC_T, KC_G)) {
      SEND_STRING("That's great");
    } else if (leader_sequence_two_keys(KC_T, KC_S)) {
      SEND_STRING("Thanks sweetie");
    } else if (leader_sequence_one_key(KC_Y)) {
      SEND_STRING("Yikes");
    } else if (leader_sequence_two_keys(KC_N, KC_F)) {
      SEND_STRING("Kyler");
    } else if (leader_sequence_two_keys(KC_N, KC_L)) {
      SEND_STRING("Johnson");
    } else if (leader_sequence_three_keys(KC_N, KC_F, KC_L)) {
      SEND_STRING("Kyler Johnson");
    } else if (leader_sequence_two_keys(KC_E, KC_M)) {
      SEND_STRING("mortifiedchaos01@gmail.com");
    } else if (leader_sequence_two_keys(KC_E, KC_K)) {
      SEND_STRING("kyler.johnson.01@gmail.com");
    } else if (leader_sequence_one_key(KC_A)) {
      SEND_STRING("19700 49 Ave");
    } else if (leader_sequence_two_keys(KC_A, KC_C)) {
      SEND_STRING("Langley");
    } else if (leader_sequence_two_keys(KC_A, KC_P)) {
      SEND_STRING("V3A 3R3");
    } else if (leader_sequence_two_keys(KC_M, KC_S)) {
      SEND_STRING("SELECT * FROM ");
    } else if (leader_sequence_two_keys(KC_W, KC_E)) {
      SEND_STRING("kjohnson@schoolathome.ca");
    } else if (leader_sequence_two_keys(KC_W, KC_U)) {
      SEND_STRING("kjohnson");
    }
}

#endif

#ifdef AUDIO_ENABLE
    if (muse_mode) {
        if (muse_counter == 0) {
            uint8_t muse_note = muse_offset + SCALE[muse_clock_pulse()];
            if (muse_note != last_muse_note) {
                stop_note(compute_freq_for_midi_note(last_muse_note));
                play_note(compute_freq_for_midi_note(muse_note), 0xF);
                last_muse_note = muse_note;
            }
        }
        muse_counter = (muse_counter + 1) % muse_tempo;
    } else {
        if (muse_counter) {
            stop_all_notes();
            muse_counter = 0;
        }
    }
#endif


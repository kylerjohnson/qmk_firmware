#include QMK_KEYBOARD_H

void keyboard_post_init_user(void) {
  rgblight_enable_noeeprom(); // enables RGB, without saving settings
  rgblight_sethsv_noeeprom(HSV_TEAL); // sets the color to teal without saving
  // rgblight_mode_noeeprom(RGBLIGHT_MODE_BREATHING + 3); // sets mode to Fast breathing without saving
}

enum custom_keycodes {
  KC_TMUX = SAFE_RANGE,
};

enum layers {
    _COLEMAK_DH,
    _NAV,
    _SYM,
    _FUNCTION,
    _MOUSE,
    // _ADJUST,
};

// Aliases for readability
#define COLEMAK  DF(_COLEMAK_DH)

#define NAV      MO(_NAV)
#define FKEYS    MO(_FUNCTION)
#define MOUSE    MO(_MOUSE)
//#define ADJUST   MO(_ADJUST)
// #define SYM_RBRC LT(_SYM, KC_RBRC)
#define SYM MO(_SYM)

#define CTL_ESC  MT(MOD_LCTL, KC_ESC)
#define CTL_QUOT MT(MOD_RCTL, KC_QUOTE)
#define CTL_MINS MT(MOD_RCTL, KC_MINUS)
#define ALT_ENT  MT(MOD_LALT, KC_ENT)

// Homerow Mods
#define LGUI_A LGUI_T(KC_A)
#define LALT_R LALT_T(KC_R)
#define LCTL_S LCTL_T(KC_S)
#define LSHFT_T LSFT_T(KC_T)

#define RSFT_N RSFT_T(KC_N)
#define RCTL_E RCTL_T(KC_E)
#define RALT_I RALT_T(KC_I)
#define RGUI_O RGUI_T(KC_O)

enum combos {
  TM_TMUX,
  NE_ESC,
  MN_MENU,
  PB_BSPC,
  JL_DEL,
  ST_TAB,
  LD_LEAD,
};

const uint16_t PROGMEM tm_tmux[] = {LSHFT_T, KC_M, COMBO_END};
const uint16_t PROGMEM ne_combo[] = {RSFT_N, RCTL_E, COMBO_END};
const uint16_t PROGMEM mn_combo[] = {KC_M, RSFT_N, COMBO_END};
const uint16_t PROGMEM pb_combo[] = {KC_P, KC_B, COMBO_END};
const uint16_t PROGMEM jl_combo[] = {KC_J, KC_L, COMBO_END};
const uint16_t PROGMEM st_combo[] = {LCTL_S, LSHFT_T, COMBO_END};
const uint16_t PROGMEM ld_combo[] = {KC_L, KC_D, COMBO_END};

combo_t key_combos[] = {
  [TM_TMUX] = COMBO(tm_tmux, KC_TMUX),
  [NE_ESC] = COMBO(ne_combo, KC_ESC),
  [MN_MENU] = COMBO(mn_combo, KC_APP),
  [PB_BSPC] = COMBO(pb_combo, KC_BSPC),
  [JL_DEL] = COMBO(jl_combo, KC_DEL),
  [ST_TAB] = COMBO(st_combo, KC_TAB),
  [LD_LEAD] = COMBO(ld_combo, QK_LEAD),
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
/*
 * Base Layer: Colemak DH
 *
 * ,----------------------------------.  ,----------------------------------.
 * |   Q  |   W  |   F  |   P  |   B  |  |   J  |   L  |   U  |   Y  | ;  : |
 * |------+------+------+------+------|  |------+------+------+------+------|
 * |   A  |   R  |   S  |   T  |   G  |  |   M  |   N  |   E  |   I  |   O  |
 * |------+------+------+------+------|  |------+------+------+------+------|
 * |   Z  |   X  |   C  |   D  |   V  |  |   K  |   H  | ,  < | . >  | /  ? |
 * `-------------+------+------+------|  |------+------+------+-------------'
 *                      | MOUSE| Space|  | SYM  | NAV  |
 *                      `-------------'  `-------------'
 */
    [_COLEMAK_DH] = LAYOUT(
      KC_Q,   KC_W,   KC_F,   KC_P,    KC_B,   KC_J, KC_L,   KC_U,    KC_Y,   KC_SCLN,
      LGUI_A, LALT_R, LCTL_S, LSHFT_T, KC_G,   KC_M, RSFT_N, RCTL_E,  RALT_I, RGUI_O ,
      KC_Z,   KC_X,   KC_C,   KC_D,    KC_V,   KC_K, KC_H,   KC_COMM, KC_DOT, KC_SLSH,
                              MOUSE,   KC_SPC, SYM , NAV
    ),
/*
 * Nav Layer: Media, navigation
 *
 * ,----------------------------------.  ,----------------------------------.
 * | WBAK | WFWD |  {   |  }   |  `   |  |  ↓   |  →   |  =   |  +   | WREF |
 * |------+------+------+------+------|  |------+------+------+------+------|
 * | Enter|CW_TOG|  [   |  ]   |  ~   |  | PgUp | PgDn | Home | End  | Quote|
 * |------+------+------+------+------|  |------+------+------+------+------|
 * |      |      |      |      |  |   |  |  ↑   |  ←   | MPLY | MPRV | MNXT |
 * `--------------------+------+------|  |------+------+--------------------'
 *                      |      |      |  |      |      |
 *                      `-------------'  `-------------'
 */
    [_NAV] = LAYOUT(
      KC_WBAK, KC_WFWD, KC_LCBR, KC_RCBR, KC_GRV,   KC_DOWN, KC_RGHT, KC_EQL,  KC_PLUS, KC_WREF,
      KC_ENT,  CW_TOGG, KC_LBRC, KC_RBRC, KC_TILD,  KC_PGUP, KC_PGDN, KC_HOME, KC_END , KC_QUOT,
      _______, _______, _______, _______, KC_PIPE,  KC_UP,   KC_LEFT, KC_MPLY, KC_MPRV, KC_MNXT,
                                 _______, _______,  _______, _______
    ),

/*
 * Sym Layer: Numbers and symbols
 *
 * ,----------------------------------.  ,----------------------------------.
 * |  1   |  2   |  3   |  4   |  5   |  |   6  |  7   |  8   |  9   |  0   |
 * |------+------+------+------+------|  |------+------+------+------+------|
 * |  !   |  @   |  #   |  $   |  %   |  |   ^  |  &   |  *   |  (   |  )   |
 * |------+------+------+------+------|  |------+------+------+------+------|
 * |   \  |  :   |  ;   |  -   |  [   |  |   ]  |  _   |  ,   |  .   |  /   |
 * `--------------------+------+------|  |------+------+--------------------'
 *                      |      |      |  |      |      |
 *                      `-------------'  `-------------'
 */
    [_SYM] = LAYOUT(
      KC_1,    KC_2,    KC_3,    KC_4,    KC_5,    KC_6,    KC_7,    KC_8,    KC_9,    KC_0 ,
      KC_EXLM, KC_AT,   KC_HASH, KC_DLR,  KC_PERC, KC_CIRC, KC_AMPR, KC_ASTR, KC_LPRN, KC_RPRN,
      KC_BSLS, KC_COLN, KC_SCLN, KC_MINS, KC_LBRC, KC_RBRC, KC_UNDS, KC_COMM, KC_DOT,  KC_SLSH,
                                 _______, _______, _______, _______
    ),

// /*
//  * Function Layer: Function keys
//  *
//  * ,-------------------------------------------.                              ,-------------------------------------------.
//  * |        |  F9  | F10  | F11  | F12  |      |                              |      |      |      |      |      |        |
//  * |--------+------+------+------+------+------|                              |------+------+------+------+------+--------|
//  * |        |  F5  |  F6  |  F7  |  F8  |      |                              |      | Shift| Ctrl |  Alt |  GUI |        |
//  * |--------+------+------+------+------+------+-------------.  ,-------------+------+------+------+------+------+--------|
//  * |        |  F1  |  F2  |  F3  |  F4  |      |      |      |  |      |      |      |      |      |      |      |        |
//  * `----------------------+------+------+------+------+------|  |------+------+------+------+------+----------------------'
//  *                        |      |      |      |      |      |  |      |      |      |      |      |
//  *                        |      |      |      |      |      |  |      |      |      |      |      |
//  *                        `----------------------------------'  `----------------------------------'
//  */
//     [_FUNCTION] = LAYOUT(
//       _______,  KC_F9 ,  KC_F10,  KC_F11,  KC_F12, _______,                                     _______, _______, _______, _______, _______, _______,
//       _______,  KC_F5 ,  KC_F6 ,  KC_F7 ,  KC_F8 , _______,                                     _______, KC_RSFT, KC_RCTL, KC_LALT, KC_RGUI, _______,
//       _______,  KC_F1 ,  KC_F2 ,  KC_F3 ,  KC_F4 , _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
//                                  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______
//     ),
//
/*
 * Mouse Layer
 *
 * ,----------------------------------.  ,----------------------------------.
 * |      | WH_L |   ↑  | WH_R | WH_U |  |      |      |      |      |      |
 * |------+------+------+------+------|  |------+------+------+------+------|
 * |LEADER|   ←  |   ↓  |   →  | WH_D |  |  M2  |  M1  | ACL2 | ACL1 | ACL0 |
 * |------+------+------+------+------|  |------+------+------+------+------|
 * |      |      | Make | Boot |      |  |      |      |      |      |      |
 * `--------------------+------+------|  |------+------+--------------------'
 *                      |      |      |  |      |      |
 *                      `-------------'  `-------------'
 */
    [_MOUSE] = LAYOUT(
      _______, KC_WH_L, KC_MS_U, KC_WH_R, KC_WH_U,  _______, _______, _______, _______, _______,
      QK_LEAD, KC_MS_L, KC_MS_D, KC_MS_R, KC_WH_D,  KC_BTN2, KC_BTN1, KC_ACL2, KC_ACL1, KC_ACL0,
      _______, _______, QK_MAKE, QK_BOOT, _______,  _______, _______, _______, _______, _______,
                                 _______, _______,  _______, _______
    ),

// /*
//  * Adjust Layer: Default layer settings, RGB
//  *
//  * ,-------------------------------------------.                              ,-------------------------------------------.
//  * |        |      |      |QWERTY|      |      |                              |DT_PRT|DT_UP |DT_DWN|      |      |        |
//  * |--------+------+------+------+------+------|                              |------+------+------+------+------+--------|
//  * |        |      |      |Dvorak|      |      |                              | TOG  | SAI  | HUI  | VAI  | MOD  |        |
//  * |--------+------+------+------+------+------+-------------.  ,-------------+------+------+------+------+------+--------|
//  * |        |      |      |Colmak|      |      |      |      |  |      |      |      | SAD  | HUD  | VAD  | RMOD |        |
//  * `----------------------+------+------+------+------+------|  |------+------+------+------+------+----------------------'
//  *                        |      |      |      |      |      |  |      |      |      |      |      |
//  *                        |      |      |      |      |      |  |      |      |      |      |      |
//  *                        `----------------------------------'  `----------------------------------'
//  */
//     //
//     // [_ADJUST] = LAYOUT(
//     //   _______, _______, _______, _______, _______, _______,                                    DT_PRNT,DT_UP  , DT_DOWN, _______,  _______, _______,
//     //   _______, _______, _______, COLEMAK, _______, _______,                                    RGB_TOG, RGB_SAI, RGB_HUI, RGB_VAI,  RGB_MOD, _______,
//     //   _______, _______, _______, _______, _______, _______,_______, _______, _______, _______, _______, RGB_SAD, RGB_HUD, RGB_VAD, RGB_RMOD, _______,
//     //                              _______, _______, _______,_______, _______, _______, _______, _______, _______, _______
//     // ),
// // /*
// //  * Layer template
// //  *
// //  * ,-------------------------------------------.                              ,-------------------------------------------.
// //  * |        |      |      |      |      |      |                              |      |      |      |      |      |        |
// //  * |--------+------+------+------+------+------|                              |------+------+------+------+------+--------|
// //  * |        |      |      |      |      |      |                              |      |      |      |      |      |        |
// //  * |--------+------+------+------+------+------+-------------.  ,-------------+------+------+------+------+------+--------|
// //  * |        |      |      |      |      |      |      |      |  |      |      |      |      |      |      |      |        |
// //  * `----------------------+------+------+------+------+------|  |------+------+------+------+------+----------------------'
// //  *                        |      |      |      |      |      |  |      |      |      |      |      |
// //  *                        |      |      |      |      |      |  |      |      |      |      |      |
// //  *                        `----------------------------------'  `----------------------------------'
// //  */
// //     [_LAYERINDEX] = LAYOUT(
// //       _______, _______, _______, _______, _______, _______,                                     _______, _______, _______, _______, _______, _______,
// //       _______, _______, _______, _______, _______, _______,                                     _______, _______, _______, _______, _______, _______,
// //       _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
// //                                  _______, _______, _______, _______, _______, _______, _______, _______, _______, _______
// //     ),
};

bool tmux_flag = false;
uint16_t previous_keycode;
uint16_t ld_combo_timer = 0;
uint8_t ld_combo_counter_25 = 0;
uint8_t ld_combo_counter_30 = 0;
uint8_t ld_combo_counter_35 = 0;
uint8_t ld_combo_counter_40 = 0;
uint8_t ld_combo_counter_45 = 0;


void update_ld_combo_counter(void) {
    uint16_t time_elapsed = timer_elapsed(ld_combo_timer);

    if (time_elapsed <= 25) {
        ld_combo_counter_25 += 1;
    } else if (time_elapsed <= 30) {
        ld_combo_counter_30 += 1;
    } else if (time_elapsed <= 35) {
        ld_combo_counter_35 += 1;
    } else if (time_elapsed <= 40) {
        ld_combo_counter_40 += 1;
    } else {
        ld_combo_counter_45 += 1;
    }
}

bool process_record_user(uint16_t keycode, keyrecord_t *record) {

  switch (keycode) {
    case KC_TMUX:
      if (record->event.pressed) {
        SEND_STRING(SS_LCTL("a"));
        set_oneshot_layer(_SYM, ONESHOT_START);
        tmux_flag = true;
      }
    case KC_L:
      if (record->event.pressed && previous_keycode == KC_D) {
        update_ld_combo_counter();
      }
      ld_combo_timer = timer_read();
      previous_keycode = keycode;
      return true;
    case KC_D:
      if (record->event.pressed && previous_keycode == KC_L) {
        update_ld_combo_counter();
      }
      ld_combo_timer = timer_read();
      previous_keycode = keycode;
      return true;
    default:
      previous_keycode = keycode;
      return true; // Process all other keycodes normally
  }
}

void post_process_record_user(uint16_t keycode, keyrecord_t *record) {
  switch (keycode) {
    case KC_TMUX:
      break;
    default:
      if(tmux_flag){
        tmux_flag = false;
        clear_oneshot_layer_state(ONESHOT_PRESSED);
      }
  }
}

#ifdef OLED_ENABLE
static void render_status(void) {
    // Host Keyboard Layer Status
    oled_write_P(PSTR("Layer"), false);
    switch (get_highest_layer(layer_state)) {
        case _COLEMAK_DH:
            oled_write_ln_P(PSTR("Base\n"), false);
            break;
        case _NAV:
            oled_write_ln_P(PSTR("Nav\n"), false);
            break;
        case _SYM:
            oled_write_ln_P(PSTR("Sym\n"), false);
            break;
        case _FUNCTION:
            oled_write_ln_P(PSTR("Fun\n"), false);
            break;
        case _MOUSE:
            oled_write_ln_P(PSTR("Mouse"), false);
            break;
        default:
            oled_write_ln_P(PSTR("Undef"), false);
    }
}

void write_ld_counters(void) {
    char ldc_str[9];

    sprintf(ldc_str, "LD25: %03d", ld_combo_counter_25);
    oled_write_ln_P(ldc_str, false);

    sprintf(ldc_str, "LD30: %03d", ld_combo_counter_30);
    oled_write_ln_P(ldc_str, false);

    sprintf(ldc_str, "LD35: %03d", ld_combo_counter_35);
    oled_write_ln_P(ldc_str, false);

    sprintf(ldc_str, "LD40: %03d", ld_combo_counter_40);
    oled_write_ln_P(ldc_str, false);

    sprintf(ldc_str, "LD45: %03d", ld_combo_counter_45);
    oled_write_ln_P(ldc_str, false);
}

bool oled_task_user(void) {

    if (is_keyboard_master()) {
        write_ld_counters();
    } else {
        render_status();
        #ifdef WPM_ENABLE
            static char wpm_str[10];
            sprintf(wpm_str, "WPM: %03d", get_current_wpm());
            oled_write_ln_P(wpm_str, false);
        #endif
    }
    return false;
}

#endif
